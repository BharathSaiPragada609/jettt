//
//  ArticlesViewController.swift
//  JetTTTask
//
//  Created by Venkateshwarlu on 7/20/20.
//  Copyright © 2020 BharathSaiPragada. All rights reserved.
//

import UIKit

class ArticlesViewController: UIViewController {
    
    @IBOutlet var articlesObject: ArticlesModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        articlesObject.aritcleTVOutlet.delegate = self
        articlesObject.aritcleTVOutlet.dataSource = self
        
        //API Call For Getting Current Article Queue
        self.getCurrentQueueArticles()
    }

    //MARK:- Getting Current Article Queue
    func getCurrentQueueArticles() {
        articlesObject.getArticlessFromServerThroughClosure(page: articlesObject.currentPage) { (response) in
            print("response in VC : ", response.count)
            DispatchQueue.main.async {
                self.articlesObject.aritcleTVOutlet.reloadData()
            }
        }
    }
}

