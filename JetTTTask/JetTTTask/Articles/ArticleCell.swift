//
//  ArticleCell.swift
//  JetTTTask
//
//  Created by Venkateshwarlu on 7/20/20.
//  Copyright © 2020 BharathSaiPragada. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {
    
    @IBOutlet weak var userIMGOutlet: UIImageView!
    @IBOutlet weak var userNameLBL: UILabel!
    @IBOutlet weak var designationLBL: UILabel!
    @IBOutlet weak var postedTimeLBL: UILabel!
    
    @IBOutlet weak var articleIMGOutlet: UIImageView!
    @IBOutlet weak var articleContentLBL: UILabel!
    @IBOutlet weak var articleTitleLBL: UILabel!
    @IBOutlet weak var articleURLLBL: UILabel!
    
    @IBOutlet weak var likesLBL: UILabel!
    @IBOutlet weak var commentsLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
