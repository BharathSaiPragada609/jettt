//
//  Articles.swift
//  JetTTTask
//
//  Created by Venkateshwarlu on 7/20/20.
//  Copyright © 2020 BharathSaiPragada. All rights reserved.
//

import Foundation

struct Articles: Decodable {
    var id: String
    var createdAt: String
    var content: String
    var comments: Int
    var likes: Int
    var media: [Media]
    var user: [User]
}

struct Media: Decodable {
    var id: String
    var blogId: String
    var createdAt: String
    var image: String
    var title: String
    var url: String
}

struct User: Decodable {
    var id: String
    var blogId: String
    var createdAt: String
    var name: String
    var avatar: String
    var lastname: String
    var city: String
    var designation: String
    var about: String
}
