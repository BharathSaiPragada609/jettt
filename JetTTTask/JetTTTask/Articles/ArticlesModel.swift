//
//  ArticlesModel.swift
//  JetTTTask
//
//  Created by Venkateshwarlu on 7/20/20.
//  Copyright © 2020 BharathSaiPragada. All rights reserved.
//

import UIKit
import Kingfisher

class ArticlesModel: NSObject {

    @IBOutlet weak var aritcleTVOutlet: UITableView!
    var responseData = [Articles]()
    var currentPage: Int = 1
    
    func getArticlessFromServerThroughClosure(page: Int, completionHandler: @escaping ([Articles]) -> Void) {
        let APIUrl = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/blogs?page=%20"+page.description+"&limit=10"
        
        print("APIUrl :",APIUrl)
        guard let url = URL(string: APIUrl) else {
            return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url, completionHandler: { (data, response, error) in
            guard let data = data else { return }
            do {
                // make sure this JSON is in the format we expect
                let response = try JSONDecoder().decode([Articles].self, from: data)
                self.responseData += response
                print("ArticlessFromServer : ",self.responseData.count)
                self.currentPage += 1
                completionHandler(self.responseData)
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
        })
        task.resume()
    }
    
}

extension ArticlesViewController : UITableViewDelegate, UITableViewDataSource {
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articlesObject.responseData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        guard let cell: ArticleCell = articlesObject.aritcleTVOutlet.dequeueReusableCell(withIdentifier: "articleCell") as? ArticleCell else {
            return UITableViewCell()
        }
          // set the text from the data model
        
        if let article = articlesObject.responseData[indexPath.row] as? Articles {
            let userObject = article.user
            let mediaObject = article.media
            DispatchQueue.main.async {
                self.prepareCellForArticle(cell: cell, article: article, userObject: userObject, mediaObject: mediaObject)
            }
        }
        // API Call for Articals Pagination
        let lastElement = articlesObject.responseData.count - 1
        if indexPath.row == lastElement {
            //call get api for next page
            self.getCurrentQueueArticles()
        }
        
        return cell
    }
    func prepareCellForArticle(cell: ArticleCell, article: Articles, userObject: [User], mediaObject: [Media]) {
        if(userObject.count>0){
            let userDict = userObject[0]
            self.cellForUserDetailsInArticle(cell: cell, userDict: userDict)
        }
        if(mediaObject.count>0){
            let mediaDict = mediaObject[0]
            self.cellForMediaDetailsInArticle(cell: cell, mediaDict: mediaDict)
        }
        cell.articleContentLBL.text = article.content
        cell.commentsLBL.text = article.comments.description
        cell.likesLBL.text = article.likes.description
    }
    func cellForUserDetailsInArticle(cell: ArticleCell, userDict: User) {
        let url = URL(string: userDict.avatar)
        cell.userIMGOutlet.kf.setImage(with: url)
        cell.userNameLBL.text = userDict.name+userDict.lastname
        cell.designationLBL.text = userDict.designation
        cell.postedTimeLBL.text = userDict.createdAt
    }

    func cellForMediaDetailsInArticle(cell: ArticleCell, mediaDict: Media) {
        if mediaDict.image != "" {
            let url = URL(string: mediaDict.image)
            cell.articleIMGOutlet.kf.setImage(with: url)
        }else{
            cell.articleIMGOutlet.isHidden = true
        }
        if mediaDict.title != "" {
            cell.articleTitleLBL.text = mediaDict.title
        }else{
            cell.articleTitleLBL.isHidden = true
        }
        if mediaDict.url != "" {
            cell.articleURLLBL.text = mediaDict.url
        }else{
            cell.articleURLLBL.isHidden = true
        }
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
}
